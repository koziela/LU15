## How does it work?

Algorithm (trading_bot_one) is trading shares of Tesla with the following rules:

+ If the current price is 3% above the 10-day average price, we open a
long position. 

+ If the current price is below the average price, then we want to close
our position to 0 shares and it needs to be in a major exchange. 

+ Algo run everyday at the market open

### Results

While using:

+ period: from 2018-02-01 to 2018-03-20
+ initial capital: $10,000,000

the return (loss) amounts to -1.66%.

### Code structure

initialize() function is called once at the very beginning of backtesting 
using context argument (which is an empty dictionary). 

Within the function, we choose the security we want to trade (here Tesla).

```def initialize(context):
    context.security = symbol('TSLA')```

In order to run the algo every day at open market we use schedule_function which
automatically run the action_function on a predetermined schedule (every day
at open market). 

```def initialize(context):
    context.security = symbol('TSLA')
    schedule_function(func = action_function, 
                      date_rule = date_rules.every_day(), 
                      time_rule = time_rules.market_open(hours=0, minutes=1) 
                     )```
                     
                     
The next step is to define action_function (the one which will be run every day
at open market).

Firstly, we need to know the prices of Tesla stock. In order to return data 
we use data.history. We indicate specific assets (context.security), fields 
(we want to get prices) and period (10 days).

```def action_function(context, data):
    price_history=data.history(context.security, 
                               fields='price', 
                               bar_count=10, 
                               frequency='1d')```

To run the algo the mean from those 10 days is needed and also the current price
of the assets (we use data.current() for our security, asking for prices).

```average_price = price_history.mean()
current_price = data.current(assets=context.security,
                                 fields='price')```

To make sure that our stocks are in a major exchange, we add a condition with
data.can_trade() which take assets and returns True if the security is currently
listed on a supported exchange and is not currently restricted.

The next conditions are opening a long position when current price of Tesla stock
is 3% above the mean (from last 10 days) and closing position to 0 shares if 
condition is not true.

```if data.can_trade(context.security):
        if current_price > average_price*1.03:
            order_target_percent(context.security,1)
        else:
            order_target_percent(context.security,0)```

## Change in the code trading_bot_two

The repo trading_bot_two contains almost the same code but the condition of 
taking long position has been changed:
+ If the current price is 2% above the 10-day average price, we open a
long position.

### Results

While using the same conditions:

+ period: from 2018-02-01 to 2018-03-20
+ initial capital: $10,000,000

the return amounts to +1.62%

## Change in the code trading_bot_three

The repo trading_bot_three contains almost the same code but the condition of 
taking long position has been changed:
+ If the current price is 3% above the 7-day average price, we open a
long position.

### Results

While using the conditions:

+ extended period: from 2017-01-01 to 2018-04-20
+ initial capital: $10,000,000

the return amounts to +8.98%
